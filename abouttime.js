// const domContainer = document.querySelector('#root');
// ReactDOM.render(e(LikeButton), domContainer);

class App extends React.Component {
    constructor(props){
        super(props);
        this.state={welcomeMessage:true, questionFilter:()=>true};
    }

    render(){
        const questions = data.filter(this.state.questionFilter);
        return (
            this.state.welcomeMessage ? 
            <div className="GameOver WelcomeMessage">
                <div className="HideSmallIframe">
                    <h2>A Fast Game About Time</h2>
                        <ul>
                            <li>This game has the quickest answers in Stack Exchange.</li>
                            <li>You have 90 seconds to answer as many as you can - but
                                you must answer faster than the actual answer was answered!</li>
                            <li>All answers are short, usually single word. Either way, don't bother with spaces or punctuation.</li>
                            <li>Click <kbd>(↵Enter)</kbd> to skip.</li>
                            <li>Keyboard required.</li>
                        </ul>
                    <h3>Choose your questions:</h3>
                    <button onClick={()=>this.setState({welcomeMessage:false,
                        questionFilter:q=>q.site==='english'})}>
                        <strong>Start</strong> - <img src="https://cdn.sstatic.net/Sites/english/img/favicon.ico?v=cafbfc112fb2" />
                        {' '}
                        English Language and Usage
                    </button>
                    <br />
                    <button onClick={()=>this.setState({welcomeMessage:false,
                        questionFilter:q=>q.site==='movies' || q.site==='scifi'})}>
                        <strong>Start</strong> - {' '}
                        <img src="https://cdn.sstatic.net/Sites/movies/img/favicon.ico?v=cafbfc112fb2" />
                        Movies &amp; TV + {' '}
                        <img src="https://cdn.sstatic.net/Sites/scifi/img/favicon.ico?v=cafbfc112fb2" /> SciFi
                    </button>
                    <br />
                    <button onClick={()=>this.setState({welcomeMessage:false})}>
                        <strong>Start</strong> - 
                        <img src="https://cdn.sstatic.net/Sites/english/img/favicon.ico?v=cafbfc112fb2" />
                        <img src="https://cdn.sstatic.net/Sites/movies/img/favicon.ico?v=cafbfc112fb2" />
                        <img src="https://cdn.sstatic.net/Sites/scifi/img/favicon.ico?v=cafbfc112fb2" />
                        {' '}
                        ALL
                    </button>
                    <hr />
                    Made by <a href='https://stackoverflow.com/users/7586/kobi' target='_blank'>Kobi</a>.
                    Source code at <a href='https://gitlab.com/kobikobi/stackoverflowabouttime' target='_blank'>GitLab</a>.
                </div>
                <div className="ShowSmallIframe">
                    Please click on <em>Full page</em><br/>
                    <small>This game requires a keyboard.</small>
                    <div className="FullPageArrow">🡅</div>
                </div>
            </div>
            :
            <Game data={questions} maxTime={questions[questions.length-1].time+1} />
        );
    }
}

class Game extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            startTimestamp: performance.now(),
            timer:0,questions:props.data,
            currentQuestionIndex:0,
            finished:false,
        }
        this.tick = this.tick.bind(this);
        this.updateQuestions = this.updateQuestions.bind(this);
        this.interval = setInterval(this.tick,3);
    }

    tick(){
        let newTimer = Math.floor(performance.now() - this.state.startTimestamp);
        let finished = newTimer>=this.props.maxTime*1000;
        let currentQuestionIndex = this.state.currentQuestionIndex;
        let current = this.state.questions[currentQuestionIndex];

        if(current.time*1000 <= newTimer){
            //failed question after it was visible.
            current.failed = true;
            current.current = false;
        }
        //find next question.
        //check if the current question is still current
        currentQuestionIndex = this.state.questions
            .findIndex(q=>!q.failed && !q.answered && !q.skipped && (q.time*1000 > newTimer));
        if(currentQuestionIndex>=0){
            this.state.questions[currentQuestionIndex].current=true;
        }else{
            finished = true;
            current.current = false;
            //last question still visible.
            if(!current.answered){current.failed=true;}
        }

        if(finished){
            clearInterval(this.interval);
        }

        this.setState({timer: newTimer, finished, questions:this.state.questions, currentQuestionIndex});
    }
    
    updateQuestions(){
        this.tick();
        //this.setState({questions:this.qu})
    }

    render(){
        const answeredCount = this.state.questions.filter(q=>q.answered).length;
        const currentQuestionTimeLeft = this.state.finished?0:
            this.state.questions[this.state.currentQuestionIndex].time*1000 - this.state.timer;
        return (
        <div className="Game">
            <div className={"Content"+(this.state.finished&&answeredCount>9?" UpvoteBg":"")}>
                {this.state.finished?
                    <div className={"GameOver" + (answeredCount?' AnsweredAny':'')}>
                        <h2>Time's Up</h2>
                        You've answered {answeredCount} question{answeredCount===1?'':'s'}{answeredCount?'!':'…'}
                    </div>
                    :
                    <CurrentQuestion question={this.state.questions[this.state.currentQuestionIndex]}
                            currentQuestionIndex={this.state.currentQuestionIndex}
                            key={'currentquestion' + this.state.currentQuestionIndex}
                            updateQuestions={this.updateQuestions}
                            timer={<ShowTime value={currentQuestionTimeLeft}/>}
                    />
                }
            </div>
            <Timeline questions={this.state.questions} maxTime={this.props.maxTime} 
                timer={this.state.timer} finished={this.state.finished} />
        </div>
        );
    }
}



class Timeline extends React.Component {
    render(){
        let progress = this.props.finished ? 100 : 
                       this.props.timer * 100 / (this.props.maxTime*1000)
        return (
        <div className="Timeline">
            
            <div className='progress' style={{width:progress+'%'}}></div>
            {this.props.questions.map((q,i)=><div key={'timeline_q'+i} 
                className={'q' + (q.time*1000<=this.props.timer ? ' passed':'')
                        + (q.skipped ? ' skipped':'')
                        + (q.answered ? ' answered':'')
                        + (q.current ? ' current':'')
                        + (q.cssClass ? ' ' + q.cssClass:'')
                    } 
                style={{left:(q.time * 100 / this.props.maxTime)+'%'}}></div>)}
        </div>);
    }
}

class CurrentQuestion extends React.Component {
    constructor(props){
        super(props);
        this.state = {enteredText:'',};
        this.onTextChange=this.onTextChange.bind(this);
        this.onClickSkip=this.onClickSkip.bind(this);
    }

    onTextChange(event) {
        const newAnswer = event.target.value;
        const normalized = newAnswer.replace(/\W+/g,'').toLowerCase();
        this.setState({enteredText: newAnswer});
        if(!newAnswer) return;
        
        let answered = this.props.question.answers.includes(normalized);
        if(answered){
            this.props.question.answered = true;
            this.props.question.current = false;
            this.props.updateQuestions();
        }
    }
    
    onClickSkip(event) {
        event.preventDefault();
        this.props.question.skipped = true;
        this.props.question.current = false;
        this.props.updateQuestions();
    }

    render(){
        const firstQuestion = this.props.currentQuestionIndex===0;
        return (
        <div className={"CurrentQuestion" + (firstQuestion?" First":"")}>
            <div className="Question">
                <h2 className="Title">
                    {this.props.question.title}
                </h2>
                {this.props.question.tags.map((t,i)=><span className="tag" key={'tag'+i}>{t}</span>)}
                <div className="questionBody" dangerouslySetInnerHTML={{__html:this.props.question.body}}>
                </div>
            </div>
                
            <form onSubmit={this.onClickSkip} className="Answer">
                <label>Your answer:
                    {' '}
                    <input type='text' className="answer" tabIndex='-1' autoFocus 
                        onChange={this.onTextChange} value={this.state.enteredText} 
                        placeholder={firstQuestion?"example. type '"+this.props.question.answers[0]+"'":""}
                        />
                </label>
                <button type='submit'>
                    Skip<br />
                    <small>(↵Enter)</small>
                </button>
                {this.props.timer}
            </form>
        </div>);
    }
}

class ShowTime extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        let timeLeft = this.props.value;
        var seconds = Math.floor(timeLeft / 1000);
        var rest = ((timeLeft % 1000) + '00').substring(0,3);
        return (
        <div className={'timer' + (seconds<=5?' short':'') + (seconds<=1?' lastchance':'') + (timeLeft===0?' finished':'')}>
            <span className="seconds">{seconds<10?'0':''}{seconds}</span>
            <span className="ms">.{rest}</span>
        </div>);
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('root')
  );
